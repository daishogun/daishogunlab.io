---
layout: page
title: プロジェクト概要
menu_title: About
menu_sort: 90
permalink: /about/
---

かつて兵庫県姫路市に、夢の交通機関とされながらも僅か8年間の運行のみで廃線となってしまった、『姫路モノレール』が存在しました。その痕跡は長らく残っていましたが、近年になり安全性の問題から少しずつ失われ始めています。

開業してから50年となる今年、姫路モノレールのなかでも最も特徴的な建造物である「大将軍駅」跡が解体されることとなりました。この建物はモノレール駅と商業施設とアパートを一体化した、当時としては画期的な建造物でした。その特徴的な外観は「アッカンベーしているマンション」と表現されたりもします。

解体工事はこの夏に始まり、もう二度とその雄姿を見ることはできなくなってしまいます。そこで、まだ資料を収集できる今のうちにできる限りの記録を行い、後世に歴史的な資料として残したいと考えております。
<br />

謝辞
----------------

### ご協力くださった皆様

このサイトの作成にあたっては次の皆様のご協力をいただきました。ここに感謝の意を表します。

* oldman 様
* 雪板＠白菜アニメ 様
* 福田成宏 様
* ZZT 様
* [その他、この活動にご寄付くださった皆様](http://japangiving.jp/p/4533#comment)
* [株式会社 瀬戸内航空写真 様](http://www.sorakara.co.jp/) （空撮協力）
<br />

### TOP画像のクレジット

* TOP画像
  * &copy;2016 OPAP-JP contributors. ([CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)) 
    * るみあ (キャラクターデザイン)
    * 絢嶺るり (キャラクター原案)
    * Butameron (写真撮影)
<br />

### 掲載画像についての注記

* 掲載画像の一部はUR都市機構様の許可を頂いて撮影したものです。
* 掲載画像の一部は姫路市主催の姫路モノレールシンポジウムの一環として撮影したものです。
* 上記の撮影許可は当プロジェクトへの支持や賛同を示すものではありません。
<br />

運営者
------

<a href="https://opap.jp/">
<img src="{{ site.baseurl }}/images/common/opap-jp-logo.svg" alt="Open Process Animation Project Japan" class="about-logo" />
</a>
Open Process Animation Project Japan (OPAP-JP) <br />
[https://opap.jp/](https://opap.jp/)


価値のある情報をアニメという手段で共有し、誰でも自由に使える形で公開するという活動をしております。

代表: <a href="https://twitter.com/k_ibuta">井二かける</a><br />
連絡先: info-opap(at)opap.jp (at)を@に置き換えて下さい。

