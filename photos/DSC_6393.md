---
layout: photo
title: 高尾アパート 屋上03
image: 'DSC_6393.JPG'
image_raw: 'DSC_6393.NEF'
license: 'CC-BY 4.0'
liecnse_url: 'https://creativecommons.org/licenses/by/4.0/deed.ja'
license_logo: 'CC-BY.svg'
credit: '©2015 OPAP-JP Butameron.'
tags:
- Photo
---
この画像はUR都市機構の許可を得て公開しています。
