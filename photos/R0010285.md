---
layout: photo
title: 大将軍駅ホーム 清掃前
image: 'R0010285.JPG'
viewer: 3D
license: 'CC-BY 4.0'
liecnse_url: 'https://creativecommons.org/licenses/by/4.0/deed.ja'
license_logo: 'CC-BY.svg'
credit: '©2015 OPAP-JP.'
tags:
- Photo
---

この写真は姫路モノレールシンポジウムの一環で撮影協力したものです。

[3D表示](https://theta360.com/s/iSCIMD8HaDzndkrG4r63HSjGW)