#!/bin/bash

pushd `dirname "$0"` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

MOGRIFY=mogrify

which $MOGRIFY >& /dev/null
if [ $? -ne 0 ]; then
    MOGRIFY="magick mogrify"
fi

cd ${SCRIPTPATH}/../photos/originals

$MOGRIFY -path ../thumbs -thumbnail 320x240^ -gravity center -extent 320x240 -quality 70 *.JPG
$MOGRIFY -path ../resized -resize 800x -quality 90 *.JPG

exit 0
